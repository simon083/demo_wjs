easypackages::packages(c("tidyverse", 'sf', 'raster','sp','dplyr','spatialEco',
                         'exactextractr','tibble', 'here', "readr",
                         "readxl", "foreign", "data.table", "rbenchmark", "benchmarkme",
                         "validate"))
setwd(here("Input_data"))
# here::here()
##To know the current storage capacity
memory.limit()

## To increase the storage capacity
# memory.limit(size=20000)
gc()
# Load data ----------------------------------------------------------------
# Yields ------------------------------------------------------------------
dat_TA_yld <- read_csv("spam2010V2r0_global_Y_TA.csv")# Yields - all technologies together, ie complete crop - Data downloaded on 1. of September 2020
dat_TI_yld <- read_csv("spam2010V2r0_global_Y_TI.csv")# Yields - Iirrigated portion of crop
dat_TH_yld <- read_csv("spam2010V2r0_global_Y_TH.csv")# Yields - rainfed high inputs portion of crop
dat_TL_yld <- read_csv("spam2010V2r0_global_Y_TL.csv")# Yields - rainfed low inputs portion of crop
dat_TS_yld <- read_csv("spam2010V2r0_global_Y_TS.csv")# Yields - rainfed subsistence portion of crop
dat_TR_yld <- read_csv("spam2010V2r0_global_Y_TR.csv")# Yields - rainfed portion of crop (= TA - TI, or TH + TL + TS)


# Production
dat_TA_pr <- read_csv("spam2010V2r0_global_P_TA.csv")
dat_TI_pr <- read_csv("spam2010V2r0_global_P_TI.csv")
dat_TH_pr <- read_csv("spam2010V2r0_global_P_TH.csv")
dat_TL_pr <- read_csv("spam2010V2r0_global_P_TL.csv")
dat_TS_pr <- read_csv("spam2010V2r0_global_P_TS.csv")
dat_TR_pr <- read_csv("spam2010V2r0_global_P_TR.csv")

# Physical area--------------------------------
dat_TA_pa <- read_csv("spam2010V2r0_global_A_TA.csv")
dat_TI_pa <- read_csv("spam2010V2r0_global_A_TI.csv")
dat_TH_pa <- read_csv("spam2010V2r0_global_A_TH.csv")
dat_TL_pa <- read_csv("spam2010V2r0_global_A_TL.csv")
dat_TS_pa <- read_csv("spam2010V2r0_global_A_TS.csv")
dat_TR_pa <- read_csv("spam2010V2r0_global_A_TR.csv")

# Harvesting area----------------------------------
dat_TA_ha <- read_csv("spam2010V2r0_global_H_TA.csv")
dat_TI_ha <- read_csv("spam2010V2r0_global_H_TI.csv")
dat_TH_ha <- read_csv("spam2010V2r0_global_H_TH.csv")
dat_TL_ha <- read_csv("spam2010V2r0_global_H_TL.csv")
dat_TS_ha <- read_csv("spam2010V2r0_global_H_TS.csv")
dat_TR_ha <- read_csv("spam2010V2r0_global_H_TR.csv")

# Functions - Yld ---------------------------------------------------------------
ChangeNames <- function(x) { ## Renaming function
  crp <- c('whea', 'rice', 'maiz', 'barl', 'pmil', 'smil', 'sorg', 'ocer', 'pota', 'swpo', 'yams', 'cass', 'orts', 'bean', 'chic', 'cowp', 'pige', 'lent', 'opul', 'soyb',
                  'grou', 'cnut', 'oilp', 'sunf', 'rape', 'sesa', 'ooil', 'sugc', 'sugb', 'cott', 'ofib', 'acof', 'rcof', 'coco', 'teas', 'toba', 'bana', 'plnt', 'trof', 'temf', 'vege', 'rest')
  names(x)[10:51] <- crp
  return(x)
} 
GatherFun <- function(k){
  k %>% gather('crop', 'valu', whea:rest)
}
# -------------------------------------------
# If no aggregartion
AggregateFun_mean <- function(z){ #for yield
 ddply(z, .(name_adm1, crop),
        function(z) mean(z$valu[z$valu!=0]))
}

AggregateFun_sum <- function(g){ #for area
  ddply(g, .(name_adm1, crop),
        function(g) sum(g$valu))
}

CollapseFun <- function(h){
  h %>% dplyr::distinct_at(vars('name_adm1', 'crop'), .keep_all = TRUE)
}

CombineFun <- function(d,s){
  full_join(d,s, by=c("name_adm1", 'crop'))
}
# -------------------------
CleanFun <- function(l){
  l %>% 
    # dplyr::rename(
    #   # value = V1,
    #   cntr = iso3) %>%
        dplyr::select(iso3, name_cntr, name_adm1, name_adm2, alloc_key, x, y, rec_type, tech_type, unit, whea:rest)
      }

TransformFun_yld <- function(d){
  p <- ChangeNames(d)
  # l <- GatherFun(p)
  # r <- AggregateFun_mean(l)
  # y <- CollapseFun(l)
  # u <- CombineFun(r,y)
  j <- CleanFun(p)
}

TransformFun_area <- function(d){
  p <- ChangeNames(d)
  # l <- GatherFun(p)
  # r <- AggregateFun_sum(l)
  # y <- CollapseFun(l)
  # u <- CombineFun(r,y)
  j <- CleanFun(p)
}


# With aggregation long -----------------------------------------------------
ChangeNames <- function(x) { ## Renaming function
  crp <- c('whea', 'rice', 'maiz', 'barl', 'pmil', 'smil', 'sorg', 'ocer', 'pota', 'swpo', 'yams', 'cass', 'orts', 'bean', 'chic', 'cowp', 'pige', 'lent', 'opul', 'soyb',
           'grou', 'cnut', 'oilp', 'sunf', 'rape', 'sesa', 'ooil', 'sugc', 'sugb', 'cott', 'ofib', 'acof', 'rcof', 'coco', 'teas', 'toba', 'bana', 'plnt', 'trof', 'temf', 'vege', 'rest')
  names(x)[10:51] <- crp
  return(x)
} 
GatherFun <- function(k){
  k %>% gather('crop', 'valu', whea:rest)
}

AggregateFun_mean <- function(z){ #for yield
  ddply(z, .(name_adm1, crop),
        function(z) mean(z$valu[z$valu!=0]))
}

AggregateFun_sum <- function(g){ #for area
  ddply(g, .(name_adm1, crop),
        function(g) sum(g$valu))
}

CollapseFun <- function(h){
  h %>% dplyr::distinct_at(vars('name_adm1', 'crop'), .keep_all = TRUE)
}

CombineFun <- function(d,s){
  full_join(d,s, by=c("name_adm1", 'crop'))
}

# CleanFun <- function(l){
  u %>% 
    dplyr::rename(value = V1) #%>%
    dplyr::select(iso3, name_cntr, name_adm1, name_adm2, 
                  alloc_key, x, y, rec_type, tech_type, 
                  unit, whea:rest)
}

TransformFun_yld <- function(d){
  p <- ChangeNames(d)
  l <- GatherFun(p)
  r <- AggregateFun_mean(l)
  y <- CollapseFun(l)
  u <- CombineFun(r,y)
  j <- CleanFun(p)
}

# TransformFun_area <- function(d){
  p <- ChangeNames(dat_TA_pd)
  l <- GatherFun(p)
  r <- AggregateFun_sum(p)
  y <- CollapseFun(l)
  u <- CombineFun(r,y)
  j <- CleanFun(p)
}


# With aggregation l spread crops  ----------------------------------------



ChangeNames <- function(x) { ## Renaming function
  crp <- c('whea', 'rice', 'maiz', 'barl', 'pmil', 'smil', 'sorg', 'ocer', 'pota', 'swpo', 'yams', 'cass', 'orts', 'bean', 'chic', 'cowp', 'pige', 'lent', 'opul', 'soyb',
           'grou', 'cnut', 'oilp', 'sunf', 'rape', 'sesa', 'ooil', 'sugc', 'sugb', 'cott', 'ofib', 'acof', 'rcof', 'coco', 'teas', 'toba', 'bana', 'plnt', 'trof', 'temf', 'vege', 'rest')
  names(x)[10:51] <- crp
  return(x)
} 

GatherFun <- function(k){
  k %>% gather('crop', 'valu', whea:rest)
}

# AggregateFun_mean <- function(z){ #for yield
#   ddply(z, .(name_adm1, crop),
#         function(z) mean(z$valu[z$valu!=0]))
# }


# }

AggregateFun_sum <- function(g){ #for area
  ddply(g, .(name_adm1, crop),
        function(g) sum(g$valu))
}

CollapseFun <- function(h){
  h %>% dplyr::distinct_at(vars('name_adm1', 'crop'), .keep_all = TRUE)
}

CombineFun <- function(d,s){
  full_join(d,s, by=c("name_adm1", 'crop'))
}

CleanFun <- function(l){
l %>% 
  # dplyr::rename(value = V1) #%>%
dplyr::select(iso3, name_adm1,
              alloc_key, x, y, rec_type, tech_type, 
              unit, whea:rest)
}

# TransformFun_yld <- function(d){
  p <- ChangeNames(d)
  # l <- GatherFun(p)
  # r <- AggregateFun_mean(l)
  # y <- CollapseFun(l)
  # u <- CombineFun(r,y)
  j <- CleanFun(u)
# }

TransformFun_area <- function(d){
p <- ChangeNames(d)
# l <- GatherFun(p)
# r <- AggregateFun_sum(p)
# y <- CollapseFun(l)
# u <- CombineFun(r,y)
j <- CleanFun(p)
}



# rm(dat_TS_yld)

# Applying transformation to all yld_tech_types
TA_yld <- TransformFun_area(dat_TA_yld)
TH_yld <- TransformFun_area(dat_TH_yld)
TI_yld <- TransformFun_area(dat_TI_yld)
TL_yld <- TransformFun_area(dat_TL_yld)
TR_yld <- TransformFun_area(dat_TR_yld)
TS_yld <- TransformFun_area(dat_TS_yld)
lst.yld <- list(TA_yld, TH_yld, TI_yld, TL_yld, TR_yld, TS_yld)
dat_yld <- do.call("rbind", lst.yld) # merging all tech types
write.csv(dat_yld, 'C:/Users/molch/OneDrive - Wageningen University & Research/PhD_WJS/Academic/RQ1/Data_analysis/Spatial_mod/Output_dat/Yield_full_cropspread.csv', row.names=T) 

# Applying transformation to production
TA_prd <- TransformFun_area(dat_TA_pd)
TH_prd <- TransformFun_area(dat_TH_pd)
TI_prd <- TransformFun_area(dat_TI_pd)
TL_prd <- TransformFun_area(dat_TL_pd)
TR_prd <- TransformFun_area(dat_TR_pd)
TS_prd <- TransformFun_area(dat_TS_pd)
lst.prd <- list(TA_prd, TH_prd, TI_prd, TL_prd, TR_prd, TS_prd)
dat_prd <- do.call("rbind", lst.prd) # merging all tech types
write.csv(dat_prd, 'C:/Users/molch/OneDrive - Wageningen University & Research/PhD_WJS/Academic/RQ1/Data_analysis/Spatial_mod/Output_dat/Production_full_cropspread.csv', row.names=T) 

#Applying transformation to physical_tech types
TA_phy <- TransformFun_area(dat_TA_pa)
TH_phy <- TransformFun_area(dat_TH_pa)
TI_phy <- TransformFun_area(dat_TI_pa)
TL_phy <- TransformFun_area(dat_TL_pa)
TR_phy <- TransformFun_area(dat_TR_pa)
TS_phy <- TransformFun_area(dat_TS_pa)
lst.phy <- list(TA_phy, TH_phy, TI_phy, TL_phy, TR_phy, TS_phy)
dat_phy <- do.call("rbind", lst.phy) # merging all tech types
write.csv(dat_phy, 'C:/Users/molch/OneDrive - Wageningen University & Research/PhD_WJS/Academic/RQ1/Data_analysis/Spatial_mod/Output_dat/Phys_area_full_cropspread.csv', row.names=T) 

#Applying transformation to physical_tech types
TA_har <- TransformFun_area(dat_TA_ha)
TH_har <- TransformFun_area(dat_TH_ha)
TI_har <- TransformFun_area(dat_TI_ha)
TL_har <- TransformFun_area(dat_TL_ha)
TR_har <- TransformFun_area(dat_TR_ha)
TS_har <- TransformFun_area(dat_TS_ha)
lst.har <- list(TA_har, TH_har, TI_har, TL_har, TR_har, TS_har)
dat_har <- do.call("rbind", lst.har) # merging all tech types
write.csv(dat_har, 'C:/Users/molch/OneDrive - Wageningen University & Research/PhD_WJS/Academic/RQ1/Data_analysis/Spatial_mod/Output_dat/Harv_area_spatial_spread_SPAM.csv', row.names=T) 

# Independent -- Merging treenuts with SPAM -------------------------------
# LOAD data from previous step (due to memory overuse)
setwd(here('Input_data', "Rectypes_full_cropspread"))
yld_dat <- read_csv('Yield_full_cropspread.csv')
prd_dat <- read_csv('Production_full_cropspread.csv')
har_dat <- read_csv('Harv_area_spatial_spread_SPAM.csv')
phy_dat <- read_csv('Phys_area_full_cropspread.csv')


# TREENUTS WITH GADM ------------------------------------------------------
# # Load and check shapefile 
# wd=setwd(here("Input_data"))
# sph.prod = read_sf(dsn = wd, layer = "GADM_treenut_rest_pd")
# sph.harv = read_sf(dsn = wd, layer = "GADM_treenut_rest_ha")
# sph.all = st_join(sph.prod, sph.harv, join = st_within)
# # write_sf(sph.all, dsn = here("Input_data", driver = "ESRI shapefile"))
# sph.prod.tib = as_tibble(sph.prod)
# sph.prod.tib = dplyr::select(sph.prod.tib, UID, GID_0, GID_1, trnt_r_)
# 
# sph.harv.tib = as_tibble(sph.harv)
# sph.harv.tib = dplyr::select(sph.harv.tib, UID, GID_0, GID_1, trnt_r_)
# 
# # loading SPAM-GADM join
# spam.gadm.link = read_sf(dsn = here("Input_data", "GADMtoSPAM", "GADM_to_SPAM_within"), 
#                          layer = "GADM_to_SPAM_within")
# # 
# # Treenuts GADM pixel  ---------------------------------------------------------------
# # CAUTION: THESE FILES ARE WRONG! THE RATIO IS WRONGLY CALULATED (not grouped by adm1 and adm0 level)
# # Treenuts are loaded from the treenuts script
# # Production
# # rbenchmark::benchmark(
# spam.gadm.link %>% 
#   as_tibble() %>% 
#   left_join(.,sph.prod.tib, by="UID") %>% 
#   left_join(.,prd_dat, by="alloc_key") %>% 
#   mutate(tnuts = trnt_r_*rest) %>% 
#   mutate(rest.1 = (1-trnt_r_)*rest) %>% 
#   dplyr::select(alloc_key, UID, GID_0.x, GID_1.x, rec_type, tech_type, unit,
#          whea:rest, tnuts, rest.1) %>% 
#   write.csv(here("Input_data", "prod_tnuts_spam_gadm.csv"))
# 
# # 
# # c=read_csv(here("Input_data", "prod_tnuts_spam_gadm.csv"))
# # 
# # Approach validation > do the numbers make sense?
# # xy=dat_join_prod %>% 
# #  group_by(tnuts) %>%
# #   top_n(n=10) %>% 
# #   dplyr::select(rest, tnuts, rest.1)
# 
# # write_csv(dat_join_prod, here("Input_data", "dat_treenut_prod.csv"))
#   # st_as_sf()
# 
# # Harvested area 
# # rbenchmark::benchmark(
# spam.gadm.link %>% 
#     as_tibble() %>% 
#     left_join(., har_dat, by="alloc_key") %>%
#     left_join(.,sph.harv.tib, by="UID") %>% 
#   mutate(tnuts = trnt_r_*rest) %>% 
#   mutate(rest.1 = (1-trnt_r_)*rest) %>% 
#   dplyr::select(alloc_key, UID, GID_0.x, GID_1.x, rec_type, tech_type, unit,
#                 whea:rest, tnuts, rest.1) %>% 
#   write.csv(here("Input_data", "harv_tnuts_spam_gadm"))
# # )
# 
# # Physical land
# # rbenchmark::benchmark(
# spam.gadm.link %>% 
#   as_tibble() %>% 
#   left_join(., phy_dat, by="alloc_key") %>%
#   left_join(.,sph.harv.tib, by="UID") %>% 
#   mutate(tnuts = trnt_r_*rest) %>% 
#   mutate(rest.1 = (1-trnt_r_)*rest) %>% 
#   dplyr::select(alloc_key, UID, GID_0.x, GID_1.x, rec_type, tech_type, unit,
#                 whea:rest, tnuts, rest.1) %>% 
#   write.csv(here("Input_data", "phys_tnuts_spam_gadm"))
# 
# # Approach: Aim is to get dinal datasets on pixel level > Adm1 level
# 1_Cbind the three datasets 
# 2_Gather crops 
# 3_Spread levels rec type
# 4_Calculate yields after unit transformation  

# # 1. Load data
# # Global 
# dat.phys = read_csv(here("Input_data", "phys_tnuts_spam_gadm.csv"))
# dat.prod = read_csv(here("Input_data", "prod_tnuts_spam_gadm,csv"))
# dat.harv = read_csv(here("Input_data", "harv_tnuts_spam_gadm.csv"))
# 
# 
# # lst.tnuts = list(dat.harv, dat.phys, dat.prod)
# dat_nutsall <- do.call("rbind", list(dat.harv, dat.phys, dat.prod))
# 
# # GLobal - Issue: Do this with the new laptop! 
# # Principle of this function is to not generate any variable as this can lead to overuse of RAM.
# do.call("rbind", list(read_csv(here("Input_data", "phys_tnuts_spam_gadm")), 
#                       read_csv(here("Input_data", "prod_tnuts_spam_gadm")), 
#                       read_csv(here("Input_data", "harv_tnuts_spam_gadm")))) %>%
#   dplyr::select(-c(rest, X1)) %>%
#   pivot_longer(names_to = "crops", values_to = "value", cols = whea:rest.1) %>% 
#   pivot_wider(-unit, names_from = "rec_type", 
#               values_from = "value" ) %>% 
#   mutate(Y = (P*1000/H))%>% #checked for result. We are transforming mt to tons when multiplying Producction by 1000 
#   mutate_at(vars(Y), ~replace(., is.nan(.), 0)) %>% 
#   rename(c("GID_0" ="GID_0.x", "GID_1"="GID_1.x")) %>% 
#   write.csv(here("Input_data", "pxl_rec_tec_gadm_spam_tnuts"))

# # SUBSETTING FOR EU-27 GADM ----------------------------------------------------------------------
# EU_iso3 = c("AUT","BEL", "BGR", "HRV", "CZE", "DNK", "EST","FIN", "FRA", "DEU", "GRC", "HUN", "IRL", "ITA","LVA", 
#             "LTU", "LUX", "MLT", "NLD", "POL", "PRT","ROU", "SVK","SVN", "ESP", "SWE", "GBR")
# write.csv(EU_iso3, here("Input_data", "EU27_iso3_lst.csv"))
# length(EU_iso3) #27 member states > the list comes from gams
# 
# # Check if the EU countries iso3 are all present in dat.phys etc.
# EU_iso3 %in% dat.phys$GID_0.x #Validation 
# dat.phys.EU = read_csv(here("Input_data", "phys_tnuts_spam_gadm"))
#   filter(dat.phys, GID_0.x %in% EU_iso3)
# 
# dat.prod.EU = read_csv(here("Input_data", "prod_tnuts_spam_gadm")) %>%
#   filter(dat.prod, GID_0.x %in% EU_iso3)
# 
# dat.harv.EU = read_csv(here("Input_data", "harv_tnuts_spam_gadm")) %>% 
# dat.harv.EU = filter(dat.harv, GID_0.x %in% EU_iso3)
# 
# 
# do.call("rbind", list(read_csv(here("Input_data", "phys_tnuts_spam_gadm")),
#                       read_csv(here("Input_data", "prod_tnuts_spam_gadm")), 
#                       read_csv(here("Input_data", "harv_tnuts_spam_gadm")))) %>% 
#   filter(GID_0.x %in% EU_iso3) %>% 
#   dplyr::select(-c(rest, X1)) %>%
#   pivot_longer(names_to = "crops", values_to = "value", cols = whea:rest.1) %>% 
#   pivot_wider(-unit, names_from = "rec_type", 
#               values_from = "value" ) %>% 
#   mutate(Y = (P*1000/H))%>% # Checked with initial spam Yield sheet (kg/ha) (original yield = 285kg/ha, here =300kg/ha) convert mt to kg Issue: Weird as according to the EU stats page aMegatonne, abbreviated as Mt, is a metric unit equivalent to 1 million (106) tonnes, or 1 billion (109) kilograms. 
#   mutate_at(vars(Y), ~replace(., is.nan(.), 0)) %>% 
#   rename(c("GID_0" ="GID_0.x", "GID_1"="GID_1.x")) %>% 
#   write.csv(here("Input_data", "pxl_rec_tec_gadm_spam_tnuts_EU27"))
# 
# # Testing the approach
# {
# # cbind dfs - testing subset
# fil.prod = dat.prod %>% top_n(n=20)
# fil.harv = dat.harv %>% top_n(n=20)
# fil.phys  = dat.phys %>% top_n(n=20)
# 
# lst.tnuts = list(fil.harv, fil.phys, fil.prod)
# do.call("rbind", list(fil.harv, fil.phys, fil.prod))
# 
# long.dat.nuts <- do.call("rbind", list(fil.harv, fil.phys, fil.prod)) %>%
#   dplyr::select(-c(X1)) %>%
#   pivot_longer(names_to = "crops", values_to = "value", cols = whea:tnuts) %>% 
#   pivot_wider(-unit, names_from = "rec_type", 
#               values_from = "value" ) %>% 
#   mutate(Y = (P*1000/H))%>% # Checked with initial spam Yield sheet (kg/ha) (original yield = 285kg/ha, here =300kg/ha) convert mt to kg Issue: Weird as according to the EU stats page aMegatonne, abbreviated as Mt, is a metric unit equivalent to 1 million (106) tonnes, or 1 billion (109) kilograms. 
#   mutate_at(vars(Y), ~replace(., is.nan(.), 0)) #%>% 
#   # rename(c("GID_0" ="GID_0.x", "GID_1"="GID_1.x")) %>% 
#   write.csv(here("Input_data", "test_rbind_fil"))
# } 





# SUBSETTING FOR EU-27 GAUL  ----------------------------------------------------------------------
EU_iso3 = c("AUT","BEL", "BGR", "HRV", "CZE", "DNK", "EST","FIN", "FRA", "DEU", "GRC", "HUN", "IRL", "ITA","LVA", 
            "LTU", "LUX", "MLT", "NLD", "POL", "PRT","ROU", "SVK","SVN", "ESP", "SWE", "GBR")
# write.csv(EU_iso3, here("Input_data", "EU27_iso3_lst.csv"))
length(EU_iso3) #27 member states > the list comes from gams

# Check if the EU countries iso3 are all present in dat.phys etc.
# EU_iso3 %in% dat.phys$iso3 #Validation if all EU27 countries are in the dfs below
dat.phys.EU = read_csv(here("Input_data", "phys_tnuts_rest_spam_gaul_FINAL.csv")) %>% #Pixel level data
  filter(iso3 %in% EU_iso3) %>% 
  mutate(alloc_key = str_sub(alloc_key, start=2)) %>% 
  dplyr::select(-c(X1_1, X1))

  
dat.prod.EU = read_csv(here("Input_data", "prod_tnuts_rest_spam_gaul_FINAL1.csv")) %>% 
filter(iso3 %in% EU_iso3)%>% 
  mutate(alloc_key = str_sub(alloc_key, start=2)) %>% 
  dplyr::select(-c(X1))


dat.harv.EU = read_csv(here("Input_data", "harv_tnuts_rest_spam_gaul_FINAL.csv")) %>% 
  filter(iso3 %in% EU_iso3) %>% 
  mutate(alloc_key = str_sub(alloc_key, start=2)) %>% 
  dplyr::select(-c(X1_1, X1))

 
dat.all.EU=  do.call("rbind", list(dat.phys.EU, dat.prod.EU, dat.harv.EU)) %>% 
  # filter(iso3 %in% EU_iso3) %>% 
  # dplyr::select(-c(X1, X1_1)) %>%
  pivot_longer(names_to = "crops", values_to = "value", cols = whea:tnuts) %>% 
  pivot_wider(-unit, names_from = "rec_type", 
              values_from = "value" ) %>% 
  mutate(Y = (P*1000/H)) #%>% # Checked with initial spam Yield sheet (kg/ha) (original yield = 285kg/ha, here =300kg/ha) convert mt to kg Issue: Weird as according to the EU stats page aMegatonne, abbreviated as Mt, is a metric unit equivalent to 1 million (106) tonnes, or 1 billion (109) kilograms. 
  mutate_at(vars(Y), ~replace(., is.nan(.), 0)) %>% 
  # rename(c("GID_0" ="GID_0.x", "GID_1"="GID_1.x")) %>% 
  write_csv(here("Input_data", "pxl_rec_tec_gaul_spam_tnuts_EU_27_corr_prod.csv")) #IMPORTANT dataset. Pixel level. Tnuts.
dat.all.EU=read_csv(here("Input_data","pxl_rec_tec_gaul_spam_tnuts_EU_27_corr_prod.csv"))

# Testing the approach
# {
  # # cbind dfs - testing subset
  # fil.prod = dat.prod %>% top_n(n=20)
  # fil.harv = dat.harv %>% top_n(n=20)
  # fil.phys  = dat.phys %>% top_n(n=20)
  # 
  # lst.tnuts = list(fil.harv, fil.phys, fil.prod)
  # x= do.call("rbind", list(fil.harv, fil.phys, fil.prod))
  # 
  # long.dat.nuts <- do.call("rbind", list(fil.harv, fil.phys, fil.prod)) %>%
  #   dplyr::select(-c(X1)) %>%
  #   pivot_longer(names_to = "crops", values_to = "value", cols = whea:rest) %>% 
  #   pivot_wider(-unit, names_from = "rec_type", 
  #               values_from = "value" ) #%>% 
  #   mutate(Y = (P*1000/H))%>% # Checked with initial spam Yield sheet (kg/ha) (original yield = 285kg/ha, here =300kg/ha) convert mt to kg Issue: Weird as according to the EU stats page aMegatonne, abbreviated as Mt, is a metric unit equivalent to 1 million (106) tonnes, or 1 billion (109) kilograms. 
  #   mutate_at(vars(Y), ~replace(., is.nan(.), 0)) %>% 
  #   rename(c("GID_0" ="GID_0.x", "GID_1"="GID_1.x")) %>% 
  #   write.csv(here("Input_data", "test_rbind_fil"))
  # } 

# Crop name mapping -------------------------------------------------------
# SPAM mapping 
nam_cat <- read_csv(here("Input_data", 'crop_names.csv')) # Loading crop name sheets - mapping SPAM -- FAO (FROM SPAM)
nam_cat <- nam_cat %>%  mutate(FAOCODE = as.double(FAOCODE)) %>% 
  rename(No_spam = names(nam_cat)[1])

GAEZ_SPAM_crp = read_csv(here("Input_data", "6-GAEZ-and-SPAM-crops-correspondence-2015-02-26.csv")) #Mapping GAEZ and SPAM (from SPAM)
GAEZ_SPAM_crp = GAEZ_SPAM_crp %>% 
  rename(No_spam = names(.)[4]) %>% 
  dplyr::select('SPAM crop long name', 'GAEZ crop', No_spam) 

# ORiginal Croplist Modeldat CIFOS
FAO_CIFOS_crp = read_csv(here("Input_data", "FAO_modeldat_2020.csv"))

# FAO spreaddsheets 
FAO_stat_control = read_csv(here("Input_data","FAOSTAT_data_12-26-2020-1.csv"))
food_cat <- read_csv(here("Input_data",'foodnonfood.csv'))# laoding food usage sheet 
food_cat = food_cat %>% 
  rename(`SPAM short name`=Crop_short)
FAOcntr <- read_csv(here("Input_data",'FAO_countries.csv')) # loading FAO country codes
FAOcntr <- dplyr::select(FAOcntr, 'ISO3', 'FAOSTAT_CODE', "GAUL_CODE", 'Continent', 'Subcontinent')


# The incomplete mapping of SPAM, GAEZ, FAOstat, FAO_Cifos are made 
# Not all the crops are matched perfectly. This is made by hand in excel (export-import a chunk below)
model_dat = left_join(FAO_CIFOS_crp, FAO_stat_control, by="crop") %>%
  left_join(nam_cat, ., by="FAOCODE") %>%
  left_join(.,GAEZ_SPAM_crp, by= "No_spam") #%>% 
  write.csv(here("Input_data", "model_crops_incomplete.csv"))

# This data frame is a complete mapping of GAEZ, SPAM and FAO (FAO original and CIFOS model dat FAO crop names)
# The FAO crop names from the model data differ slightly between Model dat and the fAO crops that I exctracted from FAOSTAT. 
# Crop others were adjusted according to SPAM indication and the GAEZ mapped crops (cereal others = oats, etc.)

dat_crp_map <- read_csv(here("Input_data","model_crops_mapping_GAEZ_SPAM_FAO_complete.csv"))
# dat_crp_map2 <- read_csv(here("Input_data", "model_crops_incomplete.csv")) 

#Validation > Result = Complete match!
# dat_crp_ma p2$ crop_cifos %in% FAO_CIFOS_crp$crop 

dat_crp_map1 = dat_crp_map %>% 
  rename(crop_cifos = crop) %>% 
  left_join(.,food_cat) #adding food categories (food/non-food)

dat_crp_map1$Usage[42]="food" #adding the food category to treenuts 
write.csv(dat_crp_map1, here("Input_data", "crop_name_mapping_complete.csv")) # this file can be used for further mapping of the crops 

# Aggregating to ADM1 level EU -----------------------------------------------
# Load and transform data 
dat.pxl.EU = read_csv(here("Input_data", "pxl_rec_tec_gaul_spam_tnuts_EU_27_corr_prod.csv"))
dat_TA_yld = read_csv(here("Input_data", "spam2010V2r0_global_Y_TA.csv")) %>% 
  dplyr::select(alloc_key,name_cntr,name_adm1, iso3, prod_level, x, y) %>% 
  mutate(alloc_key = str_sub(alloc_key, 2))


# Mapping countries
FAOcntr <- read_csv(here("Input_data", 'FAO_countries.csv'))
FAOcntr <- dplyr::select(FAOcntr, 'ISO3', 'FAOSTAT_CODE', "GAUL_CODE", 'Continent', 'Subcontinent') %>% 
  rename(iso3=ISO3)
dat_EU27 = read_csv(here("Input_data", "Country_name_mapping_incomplete.csv"))%>% 
  left_join(.,FAOcntr, by="iso3")

dat_crop_nam = read_csv(here("Input_data", "crop_name_mapping_complete.csv")) %>% 
  dplyr::select(-c(X1,X1_1)) %>% 
  rename(crops = 'SPAM short name')

# Transform pixel data so that it has SPAM variables (adm1 level) 

dat.pixel.EU = dat.pxl.EU %>%
  mutate_at(vars(alloc_key), funs(as.character)) %>%  
  left_join(.,dat_TA_yld, by="alloc_key") %>%
  dplyr::select(alloc_key, iso3.x, name_cntr.x,name_adm1.x, prod_level, iso3.x, G2008_1_ID, 
                ADM0_CODE, ADM0_NAME, ADM1_CODE, ADM1_NAME, x, y, crops, tech_type) %>% 
  rename(iso3=iso3.x,
         name_cntr=name_cntr.x,
         name_adm1=name_adm1.x) %>% 
    left_join(.,dat_crop_nam, by="crops") %>% 
  left_join(.,dat_EU27, by="iso3") %>% 
  dplyr::select(prod_level, G2008_1_ID, tech_type, CIFOS_cntr_nam, crop_cifos, crops) 

# df with prod_level and Cifos country EU
EU_iso3 = c("AUT","BEL", "BGR", "HRV", "CZE", "DNK", "EST","FIN", "FRA", "DEU", "GRC", "HUN", "IRL", "ITA","LVA", 
            "LTU", "LUX", "MLT", "NLD", "POL", "PRT","ROU", "SVK","SVN", "ESP", "SWE", "GBR")

dat_cntr_EU = dat.pxl.EU %>%
  mutate_at(vars(alloc_key), funs(as.character)) %>%
  left_join(.,dat_TA_yld, by="alloc_key") %>%
  dplyr::select(prod_level, iso3.x, name_adm1.x) %>% 
  dplyr::distinct_at(vars('prod_level', 'iso3.x', "name_adm1.x"), .keep_all = F) %>%
  rename(iso3=iso3.x, 
         name_adm1=name_adm1.x) %>% 
  filter(iso3 %in% EU_iso3) %>% 
  left_join(.,dat_EU27, by="iso3") %>% 
  dplyr::select(prod_level, iso3, name_adm1, CIFOS_cntr_nam, Continent, Subcontinent)# %>% 
  # rename(
    # iso3=iso3.x, 
       # name_adm1=name_adm1.x)

# df with crops and cifos crops
dat_crop_nam1 = read_csv(here("Input_data", "crop_name_mapping_complete.csv")) %>% 
  dplyr::select('SPAM short name', FAONAMES, crop_cifos) %>% 
  rename(crops = 'SPAM short name')

# Aggregating H,A,P and calculating Yields per Adm1 level 
dat.adm1.EU = dat.pxl.EU %>%
  mutate_at(vars(alloc_key), funs(as.character)) %>%
#   left_join(., dat.pxl.EU, by="ADM1_NAME") %>% 
# mutate_at(vars(alloc_key), funs(as.character)) %>%
  left_join(.,dat_TA_yld, by="alloc_key") %>%
  dplyr::select(iso3.x, name_cntr.x,  name_adm1.x, ADM0_NAME, ADM1_NAME, prod_level, tech_type, crops, A, H, P) %>%
  group_by(prod_level, tech_type, crops) %>% 
  summarize(across(.cols = A:P, .fns = sum))%>% #all three rec types are summed per adm1(prod_level) and crop
  mutate(Y = (P*1000/H))%>% # Checked with initial spam Yield sheet (kg/ha) (original yield = 285kg/ha, here =300kg/ha) convert mt to kg Issue: Weird as according to the EU stats page aMegatonne, abbreviated as Mt, is a metric unit equivalent to 1 million (106) tonnes, or 1 billion (109) kilograms. 
  mutate_at(vars(Y), ~replace(., is.nan(.), 0)) #%>% 

# This is the sheet that contains the parameters that are important for the GAMS input file
# It contains yield, harvested and physical area per country, adm1 unit and crop.
EU27_crp_adm1 = dat.adm1.EU %>% 
  left_join(.,dat_cntr_EU, by="prod_level") %>% 
  filter(iso3 %in% EU_iso3) %>%
  left_join(.,dat_crop_nam1, by="crops") %>% 
  dplyr::select(iso3, FAONAMES, Subcontinent, Continent, CIFOS_cntr_nam, name_adm1, crop_cifos, A, H, P, Y) %>% 
  write_csv(here("Input_data", "EU27_crp_adm1_gaul.csv")) # Final crop yield,area,etc. file. 

# Final dataset!!
EU27_crp_adm1 <- read_csv(here("Input_data", "EU27_crp_adm1_gaul.csv"))
View(EU27_crp_adm1)

length(unique(EU27_crp_adm1$iso3))
is.na(EU27_crp_adm1)#Correct! 27 countries present. 
# Continue: Why is ALB in the final dataset. Check where this went wrong 
# Validate Yields. Sum the ADM1 by hand. Check if matches 
# Berechne ob das sinn macht > 319652 rows ?!


# Then add again the GADM and get values for the SOC% >> ADd the peat non peat to it. 
# ADd usage to the sheet (or single sheet)
#

# Bringing back some SPAM geo-variables to compare them with GADM 
dat.agg.EU = dat_TA_yld %>% 
  dplyr::select(iso3, prod_level, alloc_key, x, y,name_cntr, name_adm1) %>% 
  right_join(.,dat.pxl.EU, by="alloc_key")
  

  
  
dat.agg.EU %>% dplyr::select(iso3) dat.pxl.EU %>% 
  # dplyr::select(-Y) #%>% 
  group_by(GID_1) %>% 
  summarize

max(dat.pxl.EU$Y)
summarise()

v <- validator()
summary(v)
cf <- confront(dat.pxl.EU, v)
summary(cf)
?confront



# creating a tidy table with all tech variables in one df
list.comb <- list(yld_dat, prod_dat, har_dat, phy_dat)
list.comb2=list(dat_har, dat_phy, dat_yld, da)
dat.comb <- do.call("rbind", list.comb)
# write.csv(dat.comb, 'C:/Users/molch/OneDrive - Wageningen University & Research/PhD_WJS/Academic/RQ1/Data_analysis/Spatial_mod/Output_dat/dat_comb_tidy.csv', row.names=T)





















# loading crop usage sheet --------------------------------------------------------------
setwd(here("Input_dat"))
food_cat <- read_csv('foodnonfood.csv')
# Loading crop name sheets
nam_cat <- read_csv('crop_names.csv')
# loading FAO country codes
FAOcntr <- read_csv(here("Input_data", 'FAO_countries.csv'))
FAOcntr <- dplyr::select(FAOcntr, 'ISO3', 'FAOSTAT_CODE', "GAUL_CODE", 'Continent', 'Subcontinent')

# joining all together
names(dat.comb)[8] <- "Crop_short"
names(nam_cat)[2] <- "Crop_short"
names(dat.comb)[2] <- "ISO3"

join.use <- left_join(dat.comb, food_cat, by="Crop_short")
join_FAOcntr <- left_join(join.use, FAOcntr, by="ISO3")
SPAM_dat_tidy <- left_join(join_FAOcntr, nam_cat, by='Crop_short')





AggregateFun_mean <- function(z){ #for yield
  ddply(z, .(name_adm1, crop),
        function(z) mean(z$valu[z$valu!=0]))
}

AggregateFun_sum <- function(g){ #for area
  ddply(g, .(name_adm1, crop),
        function(g) sum(g$valu))
}

CollapseFun <- function(h){
  h %>% dplyr::distinct_at(vars('name_adm1', 'crop'), .keep_all = TRUE)
}

CombineFun <- function(d,s){
  full_join(d,s, by=c("name_adm1", 'crop'))
}



# rename and reoder dataset columns
Clean2Fun <- function(x){
  i <- x %>% dplyr::rename(SPAMcrop_short=Crop_short, 
                           SPAMcrop_long=`SPAM long name`,
                           use=Usage) %>%
    dplyr::select(-c(`No. crt.`, ID))%>%
    arrange(name_adm1, SPAMcrop_short, rec_type, tech_type)
    c <- i[c('ISO3', 'name_cntr', 'name_adm1', 'alloc_key', 'FAOSTAT_CODE', 'GAUL_CODE',   'SPAMcrop_short', 'Continent', 'Subcontinent', 'SPAMcrop_long', 'FAONAMES', 'FAOCODE', 'GROUP', 'use', 'rec_type', 'tech_type', 'unit', 'valu')]
}
SPAM_dat_tidy <-  Clean2Fun(SPAM_dat_tidy)

# Save file - Option long dataset
write.csv(SPAM_dat_tidy, here("Input_dat", "Yld_tidy_all.csv"), row.names=T)

# Option2 - Harvested and Physical area horizontal
SprdFun <- function(x){
  t <- x %>% dplyr::select(-c(unit))  %>%
    spread(rec_type, valu)%>% 
    dplyr::rename(yield_kgha=Y, 
                  phys_area_ha=A,
                  harv_area_ha=H)
  c <- t[c('Continent', 'Subcontinent', 'ISO3', 'name_cntr', 'name_adm1', 'alloc_key', 'FAOSTAT_CODE', 'GAUL_CODE', 'SPAMcrop_short', 'SPAMcrop_long', 'FAONAMES', 'FAOCODE', 'GROUP', 'use', 'tech_type', 'phys_area_ha', 'harv_area_ha', 'yield_kgha')]
} 

sprd_dat <- SprdFun(SPAM_dat_tidy)
write.csv(sprd_dat_fixd, here("Input_dat","SPAMspread_all_fixd.csv"), row.names=T)


