-- !preview conn=con

-- MySQL Workbench Synchronization
-- Generated: 2021-11-08 16:03
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: simon083

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `cifos_crop`.`ndeposition` 
DROP FOREIGN KEY `idcountries1`;

ALTER TABLE `cifos_crop`.`crop` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `cifos_crop`.`countries` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `cifos_crop`.`ndeposition` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `cifos_crop`.`leaching_fraction` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `cifos_crop`.`area_scenario` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `cifos_crop`.`land_type` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `cifos_crop`.`zones` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `cifos_crop`.`yield_scenario` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `cifos_crop`.`arable_pasture_area` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `cifos_crop`.`area_yield_baseline` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ,
CHANGE COLUMN `area_ha` `area_ha` FLOAT(11) NULL DEFAULT NULL COMMENT 'yield: current yield from SPAM and Monfreda\narea:from SPAM and Monfreda\n' ;

ALTER TABLE `cifos_crop`.`area_lut_scenario` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `cifos_crop`.`ndeposition` 
ADD CONSTRAINT `idcountries1`
  FOREIGN KEY (`idcountries`)
  REFERENCES `cifos_crop`.`countries` (`idcountries`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
