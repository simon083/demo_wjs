---
title: "Data Mapping - European Cifos Data"
output: html_notebook
---
## Establishing the connection to the database 

Database name: CIFOS_model_WJS
Schema: cifos_crop

```{r}
# Connection to a structured EER diagramm
con <- dbConnect(odbc::odbc(), 
                 .connection_string = "Driver={MySQL ODBC 8.0 Unicode Driver};",
                 Server = "localhost", Database = "cifos_crop",  UID = "root", PWD = "154236w.S",
                 Port = 3306)

# Connection to a messy schema (no eer diagram)
con_crop_map <- dbConnect(odbc::odbc(), 
                 .connection_string = "Driver={MySQL ODBC 8.0 Unicode Driver};",
                 Server = "localhost", 
                 Database = "crop_mapping", 
                 UID = "root", 
                 PWD = "154236w.S",
                 Port = 3306)

```


# Loading the data sets from Database
```{r}
data_map = tbl(con_crop_map, "data_map")
data_map = as_tibble(tbl(con_crop_map, "data_map")) 
crop_disag_map = as_tibble(tbl(con_crop_map, "crop_disag_map"))
```

