# Clear workingspace
rm(list = ls(all.names = TRUE))
# gc()

# Preset
setwd('C:/Users/molch/OneDrive - Wageningen University & Research/PhD_WJS/Academic/RQ1/Data_analysis/Raw_dat/SPAM/2010/spam2010v2r0_global_yield.csv')

library(foreign)
library(tibble)
library(tidyverse)
library(sf)

# Dbf file ----------------------------------------------------------------
zp <- setwd('C:/Users/molch/OneDrive - Wageningen University & Research/PhD_WJS/Academic/RQ1/Data_analysis/Raw_dat/Soil maps/wise_05min_v12/wise_05min_v12/DBF')
dat_isr <- read.dbf('WISE5binD1.DBF')
dat_isrtbl <- as_tibble(dat_dbv)
# dat_isrtbl$SOILMAPUNI

# Loading FAO World SOil map data
xp <- setwd('C:/Users/molch/OneDrive - Wageningen University & Research/PhD_WJS/Academic/RQ1/Data_analysis/Raw_dat/Soil maps/DSMW')
dat_FAOsoil <- read.dbf('DSMW.dbf')
dat_FAOsoil_tbl <- as_tibble(dat_FAOsoil)
dat_FA0_SUID <- dplyr::rename(dat_FAOsoil_tbl, SUID = SNUM)
head(dat_FA0_SUID)

# # Joining dfs
Join_1 <- full_join(dat_FA0_SUID,dat_isrtbl, by="SUID")
Join_2 <full_join(join1, )
# 
# # FAO soil properties
# dat_general <- read.csv('Generalized_SU_Info.csv')
# dat_general <- as_tibble(dat_FAOsoil)
# head(dat_general)

# Import shapefile DSMV FAO -> so that we can georeference soil attributes
SHP_FAO <- st_read(
  "C:/Users/molch/OneDrive - Wageningen University & Research/PhD_WJS/Academic/RQ1/Data_analysis/Raw_dat/Soil maps/DSMW/DSMW.shp")
dat_FAOsoil_shp <- as_tibble(SHP_FAO)
dat_FAOsoil_shp$geometry
dat_FA0_shp_SUID <- dplyr::rename(dat_FAOsoil_tbl, SUID = SNUM)
# Join_1 <- full_join(dat_FA0_SUID,dat_isrtbl, by="SUID")

# Change SUID to SNUM, export dbf into DSMW folder, then check if you can open it in Arc. 
